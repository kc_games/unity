{
    "id": "ef0edd20-9749-46b4-b0f6-f7e826bd9b22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af8f34d8-d09e-4fdd-a85d-50eb070e6f52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef0edd20-9749-46b4-b0f6-f7e826bd9b22",
            "compositeImage": {
                "id": "aa9782cf-87fa-47ae-abd5-9f5c1e97fdfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af8f34d8-d09e-4fdd-a85d-50eb070e6f52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c897a5e6-bffb-4e7d-8d8a-84bd81758f8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af8f34d8-d09e-4fdd-a85d-50eb070e6f52",
                    "LayerId": "3baa12bf-5b9e-4308-a2d2-7ecef10a1217"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3baa12bf-5b9e-4308-a2d2-7ecef10a1217",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef0edd20-9749-46b4-b0f6-f7e826bd9b22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}