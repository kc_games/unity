{
    "id": "3d7c0591-5a3a-4aea-b061-c31f2e5ca4a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6417c23-d6d0-4fcd-aa29-8b072bd608be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d7c0591-5a3a-4aea-b061-c31f2e5ca4a0",
            "compositeImage": {
                "id": "a09d2fae-396a-423c-9e1d-85cdf789ad07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6417c23-d6d0-4fcd-aa29-8b072bd608be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f807cec-34bc-4530-a13c-15f7f10861c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6417c23-d6d0-4fcd-aa29-8b072bd608be",
                    "LayerId": "aeecdec9-9762-433d-962e-0dcd127dc383"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "aeecdec9-9762-433d-962e-0dcd127dc383",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d7c0591-5a3a-4aea-b061-c31f2e5ca4a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}