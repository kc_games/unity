{
    "id": "7713fb86-3b85-4299-a5f1-399d74b1b71b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 57,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fa971f6-fb61-4032-b1ec-d5beae3de2be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7713fb86-3b85-4299-a5f1-399d74b1b71b",
            "compositeImage": {
                "id": "75ddbefb-5a72-4452-923a-751cbc35cadb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa971f6-fb61-4032-b1ec-d5beae3de2be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a24e2ef-e5e3-4f95-9f3b-b305640a1c7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa971f6-fb61-4032-b1ec-d5beae3de2be",
                    "LayerId": "22e15764-2eef-498a-bba0-f1ab5853447d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "22e15764-2eef-498a-bba0-f1ab5853447d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7713fb86-3b85-4299-a5f1-399d74b1b71b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}