{
    "id": "078335fc-78f5-4b9e-b41a-c5a4e3cac998",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ee30ba0-068f-410f-92f5-3791f7f98ed6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "078335fc-78f5-4b9e-b41a-c5a4e3cac998",
            "compositeImage": {
                "id": "d8eda4ca-5947-45d3-ba4a-f3b756ebea2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee30ba0-068f-410f-92f5-3791f7f98ed6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69839c4f-275c-481e-9110-226f112effae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee30ba0-068f-410f-92f5-3791f7f98ed6",
                    "LayerId": "71272863-b344-4ce2-9beb-c049a1d23fc4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "71272863-b344-4ce2-9beb-c049a1d23fc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "078335fc-78f5-4b9e-b41a-c5a4e3cac998",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}